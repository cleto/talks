---
author: Cleto Martin
title: Chef overview
---

# Chef
## y otras herramientas

---

# ¿Qué es Chef?

---

# Arquitectura

### Chef server

### Chef clients

---
# Componentes

## Recipes

```
crear_usuario()

modificar_fstab()

montar_sda()

reiniciar_apache()
```

## Cookbooks

Colecciones de recetas. Por ejemplo: `apache` cookbook.

---
# Componentes

## Nodes

Un nodo que ejecuta `chef-client`.

## Roles

Agrupación de funcionalidad como unidead. Por ejemplo: `load_balancer`

## Environments

Predefinido: `_default`

---

# Mejor un ejemplo...

Queremos configurar el usuario `jbravo`, con la password `magan` y
en su home tiene que haber un archivo llamado `FAMILIA.jpg`.
